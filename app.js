//Lecture : Variables

/*
var name = 'John';
console.log("Name : ",name);

var lastName = 'Smith';
console.log("LastName : ",lastName);

var age = 26;
console.log("Age:",age);

var fullAge = true;
console.log(fullAge);
*/

//Lecture: Variable 2

/*
var name = 'John';
var age = 26;

console.log(name + age);
console.log(age + age);

var job, isMarried;

console.log(job);

job = 'teacher';
isMarried = false;

console.log(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');

age = 'thirty six';
job = 'driver';

console.log(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');


var lastName = prompt('What is the last name?');
console.log("lastName",lastName);

alert(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');
*/

//Lecture: Operator

/*
var now = 2016
var birthYear = now - 26;

birthYear = now - 26*2;

console.log("birthYear:",birthYear);

var ageJohn= 30;
var ageMark=30;

ageJohn = ageMark = (3+5)* 4 - 6;

//ageJohn = ageMark = 26 ;
//ageJohn = 26

console.log("ageJohn:",ageJohn);
console.log("ageMark:",ageMark);

ageJohn++;

console.log("ageJohn:",ageJohn);
ageMark*= 2;
console.log("ageMark:",ageMark);
ageMark = ageMark* 2;
console.log("ageMark:",ageMark);

*/

//Lecture: If Else Statements

/*

var name = 'John';
var age = 26;
var isMarried = 'no';

if(isMarried === 'yes'){
console.log(name + ' is married');
}else{
console.log(" Hopefully " + name + ' ' + ' married soon ');
}

isMarried = true;

if(isMarried){
console.log('YES!');
}else {
console.log('NO!');
}

if(23 == 23 ){
console.log("23 equal to 23 : type not checked");
}else {
console.log("23 not equal");
}

if(23 === 23 ){
console.log("23  equal to 23:type checked");
}else {
console.log("23 not equal to 23");
}

*/

//Lecture : boolean logic and switch

/*

var age = 20 ;

if(age <= 19){
console.log("John is Teenger");
} else if (age >= 20 && age <= 30 ) {
console.log("John is a Young Man");
}else {
console.log("John is a man");
}


var job = 'teacher';

job = prompt('what does John do? ');

switch (job) {

case 'teacher':
console.log("John teaches Kids.");
break;

case 'driver':
console.log("John drives a cab in Pune.");
break;

case 'cop':
console.log("John helps fight crime.");

break;

default:
console.log("John does something else");

}

*/

//Coding Chalenge : 1

/*
var johnAge = 25 ;
var JohnHeight = 175 ;

var jFriendAge = 24;
var jFriendHeight = 181;

var thirdPersonAge = 26;
var thirdPersonHeight = 172;

var jScore = JohnHeight + (5 * johnAge);
var jFriendScore = jFriendHeight + ( 5 * jFriendAge);
var thirdPersonScore = thirdPersonHeight + ( 5 * thirdPersonAge);


if(jScore > jFriendScore && jScore > thirdPersonScore){
console.log("John Wins.",jScore,jFriendScore,thirdPersonScore);
}else if (jScore === jFriendScore === thirdPersonScore) {
console.log("Match Draw :  Score is Same.",jScore,jFriendScore,thirdPersonScore);
}else if (jFriendScore > jScore && jFriendScore > thirdPersonScore) {
console.log("John Friend Wins.");
}else {
console.log(" Third Person Wins !!!!",jScore,jFriendScore,thirdPersonScore);
}

*/

//Lecture : Functions

/*

function calculateAge(yearOfBirth) {
var age = 2016 - yearOfBirth;
return age;
}

var ageJohn = calculateAge(1990);
var ageMike = calculateAge(1969);
var ageMary = calculateAge(1948);

console.log("John's Age : ",ageJohn);
console.log("Mike's Age : ",ageMike);
console.log("Mary's Age : ",ageMary);

function yearsUntilRetirement(name,year) {
var age = calculateAge(year);
var retirement = 65 - age;
if (retirement >= 0) {
console.log(name + ' retires in ' +  retirement + 'years.');
}else {
console.log(name + ' is already retired.');
}

}

yearsUntilRetirement('john',1990);
yearsUntilRetirement('Mike',1969);
yearsUntilRetirement('Mary',1948);

*/

// Lecture : Statement and Expression

/*

function someFun(par){
//code
}

var sommeFun = function(par){
//Code
}

//Expressions

3+ 4 ;
var x = 3;

//Statements
if(x === 5){
//Do Something
}

*/

//Lecture: Array
/*
var names = ['John', 'Jane', 'Mark'];
var years = new Array(1990,1969,1948);

console.log(names[2]);
names[1] = 'Ben';
console.log(names);

var john = ['John','Smith',1990,'developer',false];
john.push('blue');
john.unshift('Mr.');
john.pop();
john.shift();
console.log("john:",john);

if(john.indexOf('teacher') === -1){
console.log("John is not a teacher");
}

*/

//Lecture: Objects
/*
var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1990,
job: 'teacher',
isMarried: false
};

console.log(john);
console.log("john.lastName",john.lastName);
console.log("job",john['job']);

var xyz = 'job';
console.log(john[xyz]);

john.lastName = 'Miller';
john['job'] = 'programmer';

console.log(john);

var jane = new Object();
jane.name = 'Jane';
jane.lastName = 'Smith';
jane['yearOfBirth'] = 1969;
jane['job'] = 'retired';
jane['isMarried'] = true;

console.log(jane);


*/


//Lecture : Objects and Methods

//v1.0
/*    var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1992,
job: 'teacher',
isMarried: false,
family: ['Jane','Mark', 'Bob'],
calculateAge: function (){
return 2016 - this.yearOfBirth;
}
};

console.log(john.calculateAge());
var age = john.calculateAge();
john.age= age;

console.log(john);
*/

/*
//v2.O

var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1992,
job: 'teacher',
isMarried: false,
family: ['Jane','Mark', 'Bob'],
calculateAge: function (){
this.age = 2016 - this.yearOfBirth;
}
};

//    john.calculateAge();
console.log(john);


var mike = {
yearOfBirth: 1950,
calculateAge: function() {
this.age = 2016 - this.yearOfBirth;
}
};

mike.calculateAge();
console.log(mike);
*/

////////////////////////////////////////////////////////////


//Lecture: Loops
/*
for (var i = 0; i < 10; i++) {
console.log(i);
}

var names = ['John', 'Jane', 'Mary','Mark','Bob'];
for (var i = 0; i < names.length; i++) {
console.log(names[i]);

}

for(var i=names.length - 1; i>=0; --i){
console.log("Reverse : ",names[i]);
}

//While Loops

var i = 0;
while(i < names.length){
console.log("While loop : ",names[i]);
i++;
}

for (var i = 0; i <= 5 ; i++) {


// if(i === 3){
//     break;
// }

if(i = 3){
continue;
}

console.log(i);
}
*/

//Coding Challenge 2


/*
function printFullAges(years) {
var ages = [];
var fullAges = [];
for (var i = 0; i < years.length; i++) {
ages[i] = 2016 - years[i]
// console.log("ages:",ages[i]);
}

for(i = 0; i< ages.length ; i++){
if(ages[i]>=18){
console.log('Person ' + (i+1) + ' is ' +   ages[i] + ' years old, and is of full age.' );
fullAges.push(true);
}else {
console.log('Person ' + (i+1) + ' is ' +   ages[i] + ' years old, and is NOT of full age.' );
fullAges.push(false);
}
}
}
var years = [2001, 1985, 1994, 2014, 1973];
var full_1 = printFullAges(years);
console.log(full_1);
var full_2 = printFullAges([2012,1915,1999]);
console.log(full_2);
*/


//Lecture : Hoisting

/*
calculateAge(1965);
function calculateAge(year) {
console.log(2017 - year);
}

// retirement(1956);

var retirement = function(year) {
console.log(65 - (2016-year));
}

retirement(1990);

//Variables

// console.log(age);
var age = 23;
console.log(age);

function foo() {
var age = 65;
console.log(age);
}

foo();
console.log(age);
*/


/*Pig Game*/
/*
var scores,roundScore,gamePlaying;

  init();
// scores = [0,0];
// roundScore = 0;
// activePlayer = 0;

// dice = Math.floor(Math.random() *6) + 1;
// console.log(dice);

// document.querySelector('#current-' + activePlayer).textContent = dice;
// document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '</em>';

// var x = document.querySelector('#score-0').textContent;
// console.log(x);


// function btn() {
//Do Something Here
// }

document.querySelector('.btn-roll').addEventListener('click',function() {

  if(gamePlaying){
    //1. Random Number

    var dice = Math.floor(Math.random() * 6) + 1;
    console.log(dice);

    //2. Display the Result
    var diceDOM = document.querySelector('.dice');
    diceDOM.style.display = 'block';
    diceDOM.src= 'dice-' + dice + '.png';

    //3. Update the round score If the rolled number was not a 1

    if (dice !== 1) {
      //Add Score
      roundScore +=dice;
      document.querySelector('#current-' + activePlayer).textContent = roundScore;
    } else {
      //Next Player
      nextPlayer();
    }
  }


});

document.querySelector('.btn-hold').addEventListener('click',function() {
  if(gamePlaying){
    //Add CURRENT Score to Global Score
    scores[activePlayer] += roundScore;

    //Update the UI
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

    //Check if Player won the game
     if(scores[activePlayer] >= 100){
       console.log("Indide if");
       document.querySelector('#name-' + activePlayer).textContent = 'Winner' ;
       document.querySelector('.dice').style.display = 'none';
       document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
       document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
       gamePlaying = false;
     }else {
       //Next Player
       nextPlayer();
     }
    //Next Player
      nextPlayer();
  }

});

function nextPlayer() {
  //Next Player
  activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
  roundScore = 0;

  document.getElementById('current-0').textContent = 0;
  document.getElementById('current-1').textContent = 0;

  document.querySelector('.player-0-panel').classList.toggle('active');
  document.querySelector('.player-1-panel').classList.toggle('active');



  // document.querySelector('.player-0-panel').classList.remove('active');
  // document.querySelector('.player-1-panel').classList.add('active');

  document.querySelector('.dice').style.display = 'none';
}

document.querySelector('.btn-new').addEventListener('click',init);

function init(){
  scores = [0,0];
  activePlayer = 0;
  roundScore = 0;
  gamePlaying = true;
  document.querySelector('.dice').style.display = 'none';
  document.getElementById('score-0').textContent = '0';
  document.getElementById('score-1').textContent = '0';
  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';
  document.getElementById('name-0').textContent = 'Player 1';
  document.getElementById('name-1').textContent = 'Player 2';
  document.querySelector('.player-0-panel').classList.remove('winner');
  document.querySelector('.player-1-panel').classList.remove('winner');
  document.querySelector('.player-0-panel').classList.remove('active');
  document.querySelector('.player-1-panel').classList.remove('active');
  document.querySelector('.player-0-panel').classList.add('active');
}

*/
/* Challenges

1. A Player looses his Entire score when he rolls two 6 in a row. After that, it's the next player's turn.
2. Add a input field to the HTML where player can set the winning score, so that they can change the predefined score of 100,
3. Add Another dice to the game, so that there are two dices now. The player looses his curent score when one of them is a 1.

*/


  // Lecture : let and const

  //ES 5
/*
  var name5 = 'Jane Smith';
  var age5 = '23';
  name5 = 'Jane Miller';

  console.log(name5);

  //ES 6

  const name6 = 'Jane Smith';
  let age6 = '23';
  // name6 = 'Jane Miller';
  // console.log(name6);
*/

// Es5
  /*function driverLicience5(passedTest) {
    if(passedTest){
      var firstName = 'John';
      var yearOfBirth = 1990;


    }

    console.log(firstName + ' born in ' + yearOfBirth + ' is now officially allowed to drive a car ');

  }

  driverLicience5(true);

//ES6

function driverLicience6(passedTest) {
  let firstName;
  const yearOfBirth = 1990;

  if(passedTest){
    firstName = 'John';
  }

  console.log(firstName + ' born in ' + yearOfBirth + ' is now officially allowed to drive a car ');
}

driverLicience6(true);



let i = 23;
for (let i = 0; i < 5; i++) {
  console.log(i);
}

console.log(i);
*/

//Lecture : Blocks and IIFE's

//ES 6
/*
{
   const a = 1;
   let b = 2;
   console.log( a + b );
   var c = 3
}

console.log(c);
//ES5

(function() {
   var c = 3;
   console.log(c);
})();

// console.log(c);
*/


//Lecture : Strings
/*
let firstName = 'john';
let lastName = 'Smith';

const yearOfBirth = 1990;

function calcAge(year) {
   return 2017 - year;

}

//Es5

console.log('This is ' + firstName + ' ' + lastName + ' . He was born in ' + yearOfBirth + ' . Today, he is ' + calcAge(yearOfBirth) + ' years old.');

//ES6

console.log(`This is ${firstName} ${lastName}. He was born in ${yearOfBirth}. Today, he is ${calcAge(yearOfBirth)} years old.`);



const n = `${firstName} ${lastName}`;
console.log(n.startsWith('j'));
console.log(n.endsWith('h'));
console.log(n.includes('oh'));
console.log(`${firstName} `. repeat(5));
*/


//Lecture : Arrow Function

/*
const years = [1990, 1965, 1982, 1937];

//Es5

var age5 = years.map(function(el) {
   return 2017 - el;
});

console.log(age5);

//ES6

let ages6 = years.map(el => 2017 - el);

console.log(ages6);

ages6 = years.map((el, index) => `Age element ${index + 1} : ${2016 - el}.`);

console.log(ages6);

ages6 = years.map((el,index) => {

   const now = new Date().getFullYear();
   const age = now - el;
   return `Age element ${index + 1} : ${age}.`
});

console.log(ages6);

*/

//Lecture: Arrow Lecture 2

//ES5

var box5 = {
   color: 'green',
   position: 1,
   clickMe: function() {
      var self = this;
      document.querySelector('.green').addEventListener('click',function() {
         var str = 'This is box number ' + self.position + ' and it is ' + self.color;
         alert(str);
      });
   }
}

// box5.clickMe();

//ES6

var box6 = {
   color: 'green',
   position: 1,
   clickMe: function() {
      // var self = this;
      document.querySelector('.green').addEventListener('click',() => {
         let str = 'This is box number ' + this.position + ' and it is ' + this.color;
         alert(str);
      });
   }
}

// box6.clickMe();
/*
var box66 = {
   color: 'green',
   position: 1,
   clickMe: () => {
      // var self = this;
      document.querySelector('.green').addEventListener('click',() => {
         let str = 'This is box number ' + this.position + ' and it is ' + this.color;
         alert(str);
      });
   }
}

box66.clickMe();
*/

//ES5
/*
function Person(name) {
   this.name = name;
}

Person.prototype.myFriends5 = function(friends) {

   var arr = friends.map(function(el) {
      return this.name + ' is friends with ' + el;
   }.bind(this));

   console.log(arr);
}

var friends = ['Bob', 'Jane', 'Mark'];

new Person('John').myFriends5(friends);

//ES6

Person.prototype.myFriends5 = function(friends) {

   var arr = friends.map(el => `${this.name} is friends with ${el}`);
   console.log(arr);
}
new Person('John').myFriends5(friends);
*/

//Lecture : Destructuring

//ES5

var john = ['John', 26];
// var name = john[0];
// var age = john[1];

//ES6
/*
const [name, age] = ['john', 26];
console.log(name);
console.log(age);


const obj = {
   firstName: 'John',
   lastName: 'Smith'
};

const {firstName, lastName} = obj;

console.log(firstName);
console.log(lastName);

const {firstName: a, lastName: b} = obj;

console.log(a);
console.log(b);
*/
/*
function calcAgeRetirement(year) {
   const age = new Date().getFullYear() - year;
   return [age, 65 - age];
}

const[age2, retirement] = calcAgeRetirement(1990);
console.log(age2);
console.log(retirement);
*/

//Lecture : Arrays
/*
const boxes = document.querySelectorAll('.box');

//ES5

var boxesArr5 = Array.prototype.slice.call(boxes);

boxesArr5.forEach(function(cur) {
   cur.style.backgroundColor = 'dodgerblue';
});


//ES6

const boxesArr6 = Array.from(boxes);
Array.from(boxes).forEach(cur =>
cur.style.backgroundColor = 'dodgerblue');
*/

//Loops

/*
forEach,Map ---- break and continue is not used.So we use simple for loop in es5.
*/

//ES5
/*
for (var i = 0; i < boxesArr5.length; i++) {
   if(boxesArr5[i].className === 'box blue'){
      // continue;
      break;
   }

   boxesArr5[i].textContent = 'I changed to blue';
}*/

//ES6
/*
for (const cur of boxesArr6) {
   if (cur.className.includes('blue')) {
      continue;
   }

   cur.textContent = 'I changed to blue!';
}


//ES5
var ages = [12,17,8,21,14,11];

var full = ages.map(function(cur){
   return cur >= 18;
});

console.log(full);
console.log(full.indexOf(true));

console.log(ages[full.indexOf(true)]);

//ES6
console.log(ages.findIndex(cur => cur >= 18));
console.log(ages.find(cur => cur >= 18));

*/
/*
function addFourAges(a, b, c, d) {
   return a + b + c + d;
}

var sum1 = addFourAges(18, 30, 12, 21);
console.log(sum1);

//ES5
var ages = [18, 30, 12, 21];
var sum2 = addFourAges.apply(null, ages);
console.log(sum2);

//ES6

const sum3 = addFourAges(...ages);
console.log(81);

const familySmith = ['Jane', 'John', 'Mark'];

const familyMiller = ['Mary','Bob','Ann'];

const bigFamily = [... familySmith, ...familyMiller];

console.log(bigFamily);


const h = document.querySelector('h1');
const boxes2 = document.querySelectorAll('.box');

const all = [h, ...boxes2];
console.log("all",all);

Array.from(all).forEach(cur =>
cur.style.color = 'purple');
*/

// Lecture : Rest Parameters

//ES5
/*
function isFullAge5() {
   // console.log(arguments);
   var argsArr = Array.prototype.slice.call(arguments);
   argsArr.forEach(function(cur) {
      console.log((2016 - cur) >= 18);
   })
}

// isFullAge5(1990, 1999, 2005,2016,1987);

//ES6

function isFullAge6(...years) {
   console.log(years);
   for(let year of years){
      return console.log(year >= (2016 - year) );
   }
}

isFullAge6(1990, 1999, 2005,2016,1987);
*/

// Lecture : default Parameters

//ES5
/*
function SmithPerson(firstName, yearOfBirth, lastName, nationality) {
   lastName === undefined ? lastName = 'Smith' : lastName = lastName;
   nationality === undefined ? nationality = 'americam' : nationality = nationality;
   this.firstName = firstName;
   this.lastName = lastName;
   this.yearOfBirth = yearOfBirth;
   this.nationality = nationality;

}

var john = new SmithPerson('John', 1990);
var emily = new SmithPerson('emily',1983,'Diaz','spanish');
*/

//ES6

/*
function SmithPerson(firstName,yearOfBirth,lastName = 'Smith' , nationality = 'americam') {
   this.firstName = firstName;
   this.lastName = lastName;
   this.yearOfBirth = yearOfBirth;
   this.nationality = nationality;
}

var john = new SmithPerson('John', 1990);
var emily = new SmithPerson('emily',1983,'Diaz','spanish');
*/

//Lecture : Maps
/*
const question = new Map();
question.set('question','What is the oddicial name of the latest major Javascript Version');

question.set(1, 'ES5');
question.set(2, 'ES6');
question.set(3, 'ES2015');
question.set(4, 'ES7');
question.set('correct', 3);
question.set(true, 'Correct Answer : D');
question.set(false, 'Wrong, Please try again!');
// console.log(question.get('question'));
// console.log(question.size);
if(question.has(4)){
// question.delete(4);
// console.log("Inside");
}

console.log(question);

// question.forEach((value,key) =>
// console.log(`This is ${key}, and its set to ${value}`));

for(let [key,value] of question.entries()){
   // console.log(`This is ${key}, and its set to ${value}`);
   if(typeof(key) === 'number') {
      console.log(`Answer ${key} : ${value}`);
   }

}

const ans = parseInt(prompt('Write the correct answer'));

console.log(question.get(ans === question. get('correct')));
*/

//Lecture : Classes

//ES5

/*
var Person5 = function(name, yearOfBirth, job) {
   this.name = name;
   this.yearOfBirth = yearOfBirth;
   this.job = job;
}

Person5.prototype.calculateAge = function() {
   var age = new Date().getFullYear - this.yearOfBirth;
   console.log(age);
}

var john5 = new Person5('John', 1990, 'Teacher');

//ES6

class Person6 {
   constructor(name, yearOfBirth, job){
      this.name = name;
      this.yearOfBirth = yearOfBirth;
      this.job = job;
   }

   calculateAge(){
      var age = new Date().getFullYear - this.yearOfBirth;
      console.log(age);
   }

   static greeting(){
      console.log('Hey there!');
   }
}

const john6 = new Person6('John', 1990, 'teacher');
Person6.greeting();
*/

var Person5 = function(name, yearOfBirth, job) {
   this.name = name;
   this.yearOfBirth = yearOfBirth;
   this.job = job;
}

Person5.prototype.calculateAge = function() {
   var age = new Date().getFullYear() - this.yearOfBirth;
   console.log(age);
}

var Athelete5 = function(name, yearOfBirth, job,OlympicGames, medals) {
  Person5.call(this, name, yearOfBirth, job);
  this.OlympicGames = OlympicGames;
  this.medals = medals;
}


Athelete5.prototype = Object.create(Person5.prototype);

Athelete5.prototype.wonMedal = function () {
   this.medals++;
   console.log(this.medals);
}

var johnAthelete5 = new Athelete5('John', 1990, 'Swimmer', 3, 10);

johnAthelete5.calculateAge();
johnAthelete5.wonMedal();


class Person6 {
   constructor(name, yearOfBirth, job){
      this.name = name;
      this.yearOfBirth = yearOfBirth;
      this.job = job;
   }

   calculateAge(){
      var age = new Date().getFullYear() - this.yearOfBirth;
      console.log(age);
   }

}

class Athelete6 extends Person6 {

   constructor(name,yearOfBirth, job, OlympicGames, medals) {
      super(name, yearOfBirth, job);
      this.OlympicGames = OlympicGames;
      this.medals = medals;
   }

   wonMedal(){
      this.medals++;
      console.log(this.medals);
   }

}

const johnAthelete6 =new Athelete6('John', 1990, 'swimmer',3, 10);

johnAthelete6.wonMedal();
johnAthelete6.calculateAge();
