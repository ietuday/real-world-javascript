//Lecture : Variables

/*
var name = 'John';
console.log("Name : ",name);

var lastName = 'Smith';
console.log("LastName : ",lastName);

var age = 26;
console.log("Age:",age);

var fullAge = true;
console.log(fullAge);
*/

//Lecture: Variable 2

/*
var name = 'John';
var age = 26;

console.log(name + age);
console.log(age + age);

var job, isMarried;

console.log(job);

job = 'teacher';
isMarried = false;

console.log(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');

age = 'thirty six';
job = 'driver';

console.log(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');


var lastName = prompt('What is the last name?');
console.log("lastName",lastName);

alert(name + ' is a ' + age + ' years old ' + job + ' . Is he married?' + ' '+ isMarried + '.');
*/

//Lecture: Operator

/*
var now = 2016
var birthYear = now - 26;

birthYear = now - 26*2;

console.log("birthYear:",birthYear);

var ageJohn= 30;
var ageMark=30;

ageJohn = ageMark = (3+5)* 4 - 6;

//ageJohn = ageMark = 26 ;
//ageJohn = 26

console.log("ageJohn:",ageJohn);
console.log("ageMark:",ageMark);

ageJohn++;

console.log("ageJohn:",ageJohn);
ageMark*= 2;
console.log("ageMark:",ageMark);
ageMark = ageMark* 2;
console.log("ageMark:",ageMark);

*/

//Lecture: If Else Statements

/*

var name = 'John';
var age = 26;
var isMarried = 'no';

if(isMarried === 'yes'){
console.log(name + ' is married');
}else{
console.log(" Hopefully " + name + ' ' + ' married soon ');
}

isMarried = true;

if(isMarried){
console.log('YES!');
}else {
console.log('NO!');
}

if(23 == 23 ){
console.log("23 equal to 23 : type not checked");
}else {
console.log("23 not equal");
}

if(23 === 23 ){
console.log("23  equal to 23:type checked");
}else {
console.log("23 not equal to 23");
}

*/

//Lecture : boolean logic and switch

/*

var age = 20 ;

if(age <= 19){
console.log("John is Teenger");
} else if (age >= 20 && age <= 30 ) {
console.log("John is a Young Man");
}else {
console.log("John is a man");
}


var job = 'teacher';

job = prompt('what does John do? ');

switch (job) {

case 'teacher':
console.log("John teaches Kids.");
break;

case 'driver':
console.log("John drives a cab in Pune.");
break;

case 'cop':
console.log("John helps fight crime.");

break;

default:
console.log("John does something else");

}

*/

//Coding Chalenge : 1

/*
var johnAge = 25 ;
var JohnHeight = 175 ;

var jFriendAge = 24;
var jFriendHeight = 181;

var thirdPersonAge = 26;
var thirdPersonHeight = 172;

var jScore = JohnHeight + (5 * johnAge);
var jFriendScore = jFriendHeight + ( 5 * jFriendAge);
var thirdPersonScore = thirdPersonHeight + ( 5 * thirdPersonAge);


if(jScore > jFriendScore && jScore > thirdPersonScore){
console.log("John Wins.",jScore,jFriendScore,thirdPersonScore);
}else if (jScore === jFriendScore === thirdPersonScore) {
console.log("Match Draw :  Score is Same.",jScore,jFriendScore,thirdPersonScore);
}else if (jFriendScore > jScore && jFriendScore > thirdPersonScore) {
console.log("John Friend Wins.");
}else {
console.log(" Third Person Wins !!!!",jScore,jFriendScore,thirdPersonScore);
}

*/

//Lecture : Functions

/*

function calculateAge(yearOfBirth) {
var age = 2016 - yearOfBirth;
return age;
}

var ageJohn = calculateAge(1990);
var ageMike = calculateAge(1969);
var ageMary = calculateAge(1948);

console.log("John's Age : ",ageJohn);
console.log("Mike's Age : ",ageMike);
console.log("Mary's Age : ",ageMary);

function yearsUntilRetirement(name,year) {
var age = calculateAge(year);
var retirement = 65 - age;
if (retirement >= 0) {
console.log(name + ' retires in ' +  retirement + 'years.');
}else {
console.log(name + ' is already retired.');
}

}

yearsUntilRetirement('john',1990);
yearsUntilRetirement('Mike',1969);
yearsUntilRetirement('Mary',1948);

*/

// Lecture : Statement and Expression

/*

function someFun(par){
//code
}

var sommeFun = function(par){
//Code
}

//Expressions

3+ 4 ;
var x = 3;

//Statements
if(x === 5){
//Do Something
}

*/

//Lecture: Array
/*
var names = ['John', 'Jane', 'Mark'];
var years = new Array(1990,1969,1948);

console.log(names[2]);
names[1] = 'Ben';
console.log(names);

var john = ['John','Smith',1990,'developer',false];
john.push('blue');
john.unshift('Mr.');
john.pop();
john.shift();
console.log("john:",john);

if(john.indexOf('teacher') === -1){
console.log("John is not a teacher");
}

*/

//Lecture: Objects
/*
var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1990,
job: 'teacher',
isMarried: false
};

console.log(john);
console.log("john.lastName",john.lastName);
console.log("job",john['job']);

var xyz = 'job';
console.log(john[xyz]);

john.lastName = 'Miller';
john['job'] = 'programmer';

console.log(john);

var jane = new Object();
jane.name = 'Jane';
jane.lastName = 'Smith';
jane['yearOfBirth'] = 1969;
jane['job'] = 'retired';
jane['isMarried'] = true;

console.log(jane);


*/


//Lecture : Objects and Methods

//v1.0
/*    var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1992,
job: 'teacher',
isMarried: false,
family: ['Jane','Mark', 'Bob'],
calculateAge: function (){
return 2016 - this.yearOfBirth;
}
};

console.log(john.calculateAge());
var age = john.calculateAge();
john.age= age;

console.log(john);
*/

/*
//v2.O

var john = {
name: 'John',
lastName: 'Smith',
yearOfBirth: 1992,
job: 'teacher',
isMarried: false,
family: ['Jane','Mark', 'Bob'],
calculateAge: function (){
this.age = 2016 - this.yearOfBirth;
}
};

//    john.calculateAge();
console.log(john);


var mike = {
yearOfBirth: 1950,
calculateAge: function() {
this.age = 2016 - this.yearOfBirth;
}
};

mike.calculateAge();
console.log(mike);
*/

////////////////////////////////////////////////////////////


//Lecture: Loops
/*
for (var i = 0; i < 10; i++) {
console.log(i);
}

var names = ['John', 'Jane', 'Mary','Mark','Bob'];
for (var i = 0; i < names.length; i++) {
console.log(names[i]);

}

for(var i=names.length - 1; i>=0; --i){
console.log("Reverse : ",names[i]);
}

//While Loops

var i = 0;
while(i < names.length){
console.log("While loop : ",names[i]);
i++;
}

for (var i = 0; i <= 5 ; i++) {


// if(i === 3){
//     break;
// }

if(i = 3){
continue;
}

console.log(i);
}
*/

//Coding Challenge 2


/*
function printFullAges(years) {
var ages = [];
var fullAges = [];
for (var i = 0; i < years.length; i++) {
ages[i] = 2016 - years[i]
// console.log("ages:",ages[i]);
}

for(i = 0; i< ages.length ; i++){
if(ages[i]>=18){
console.log('Person ' + (i+1) + ' is ' +   ages[i] + ' years old, and is of full age.' );
fullAges.push(true);
}else {
console.log('Person ' + (i+1) + ' is ' +   ages[i] + ' years old, and is NOT of full age.' );
fullAges.push(false);
}
}
}
var years = [2001, 1985, 1994, 2014, 1973];
var full_1 = printFullAges(years);
console.log(full_1);
var full_2 = printFullAges([2012,1915,1999]);
console.log(full_2);
*/


//Lecture : Hoisting

/*
calculateAge(1965);
function calculateAge(year) {
console.log(2017 - year);
}

// retirement(1956);

var retirement = function(year) {
console.log(65 - (2016-year));
}

retirement(1990);

//Variables

// console.log(age);
var age = 23;
console.log(age);

function foo() {
var age = 65;
console.log(age);
}

foo();
console.log(age);
*/


/*Pig Game*/

var scores,roundScore,gamePlaying;
var lastDice;
init();
// scores = [0,0];
// roundScore = 0;
// activePlayer = 0;

// dice = Math.floor(Math.random() *6) + 1;
// console.log(dice);

// document.querySelector('#current-' + activePlayer).textContent = dice;
// document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '</em>';

// var x = document.querySelector('#score-0').textContent;
// console.log(x);


// function btn() {
//Do Something Here
// }

document.querySelector('.btn-roll').addEventListener('click',function() {

  if(gamePlaying){
    //1. Random Number

    var dice1 = Math.floor(Math.random() * 6) + 1;
    var dice2 = Math.floor(Math.random() * 6) + 1;

    console.log(dice);

    //2. Display the Result
    // var diceDOM = document.querySelector('.dice');
    document.getElementById('dice-1').style.display = 'block';
    document.getElementById('dice-2').style.display = 'block';
    document.getElementById('dice-1').src= 'dice-' + dice1 + '.png';
    document.getElementById('dice-1').src= 'dice-' + dice2 + '.png';

    //3. Update the round score If the rolled number was not a 1
    if (dice1 !== 1 && dice2 !== 1) {
      //Add Score
      roundScore += dice1 + dice2;
      document.querySelector('#current-' + activePlayer).textContent = roundScore;
    } else {
      //Next Player
      nextPlayer();
    }
  }
  /*  if(dice === 6 && lastDice === 6 ){
      console.log("Got Two times 6");
      //Player Looses score
      scores[activePlayer] = 0;
      document.querySelector('#score-' + activePlayer).textContent = '0';
      nextPlayer();
    }else if (dice !== 1) {
        //Add Score
        roundScore +=dice;
        document.querySelector('#current-' + activePlayer).textContent = roundScore;
      } else {
        //Next Player
        nextPlayer();
      }
    lastDice = dice;
  }*/


});

document.querySelector('.btn-hold').addEventListener('click',function() {
  if(gamePlaying){
    //Add CURRENT Score to Global Score
    scores[activePlayer] += roundScore;

    //Update the UI
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

    var input = document.querySelector('.final-score').value;
    console.log(input);
    var winningScore;
    //undefined,0,null or "" are COERCED yo false
    //Anything else is COERCED to true
    if(input){
      winningScore = input;
    }else{
      winningScore = 100;
    }
    //Check if Player won the game
     if(scores[activePlayer] >= winningScore){
       console.log("Indide if");
       document.querySelector('#name-' + activePlayer).textContent = 'Winner' ;
       document.getElementById('dice-1').style.display = 'none';
       document.getElementById('dice-2').style.display = 'none';
       document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
       document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
       gamePlaying = false;
     }else {
       //Next Player
       nextPlayer();
     }
    //Next Player
      nextPlayer();
  }

});

function nextPlayer() {
  //Next Player
  activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
  roundScore = 0;

  document.getElementById('current-0').textContent = 0;
  document.getElementById('current-1').textContent = 0;

  document.querySelector('.player-0-panel').classList.toggle('active');
  document.querySelector('.player-1-panel').classList.toggle('active');



  // document.querySelector('.player-0-panel').classList.remove('active');
  // document.querySelector('.player-1-panel').classList.add('active');

  document.getElementById('dice-1').style.display = 'none';
  document.getElementById('dice-2').style.display = 'none';

}

document.querySelector('.btn-new').addEventListener('click',init);

function init(){
  scores = [0,0];
  activePlayer = 0;
  roundScore = 0;
  gamePlaying = true;
  document.getElementById('dice-1').style.display = 'none';
  document.getElementById('dice-2').style.display = 'none';
  document.getElementById('score-0').textContent = '0';
  document.getElementById('score-1').textContent = '0';
  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';
  document.getElementById('name-0').textContent = 'Player 1';
  document.getElementById('name-1').textContent = 'Player 2';
  document.querySelector('.player-0-panel').classList.remove('winner');
  document.querySelector('.player-1-panel').classList.remove('winner');
  document.querySelector('.player-0-panel').classList.remove('active');
  document.querySelector('.player-1-panel').classList.remove('active');
  document.querySelector('.player-0-panel').classList.add('active');
}


/* Challenges

1. A Player looses his Entire score when he rolls two 6 in a row. After that, it's the next player's turn.
2. Add a input field to the HTML where player can set the winning score, so that they can change the predefined score of 100,
3. Add Another dice to the game, so that there are two dices now. The player looses his curent score when one of them is a 1.

*/
